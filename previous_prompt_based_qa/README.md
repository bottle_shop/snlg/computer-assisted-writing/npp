# Soju - Prompt-Based CQA


# Installation

```bash
conda create --name riddleqa python=3.7
conda activate riddleqa
pip install -r requirments.txt

```

# Experiments  (RS-->RS) 
Create training dataset (splitted into instance and label file.)
```bash
python scripts/polish_riddle.py
```

Train
```bash
python train.py model=bert-large
python train.py model=roberta-large
python train.py model=albert-xxl     
```
