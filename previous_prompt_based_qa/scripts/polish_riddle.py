import argparse
from collections import Counter, defaultdict
import itertools
import json
from pathlib import Path
import random

def main() -> None:
    p = argparse.ArgumentParser(description=__doc__)
    p.add_argument('--input-dir', type=Path, default=Path('data_dir','riddle_sense'))
    p.add_argument('--output-dir', type=Path, default=Path('task_data', 'riddle_sense'))
    p.add_argument('--subset', type=float, default=None)
    args = p.parse_args()

    for in_split, out_split in (('train', 'train'), ('dev', 'dev'), ('test', 'test')):
        instance_path = args.input_dir / f'{in_split}.jsonl'

        with instance_path.open(encoding='utf-8-sig') as file:
            instances = [json.loads(line) for line in file]

        #print(f'{in_split}: {Counter(instance["question"] for instance in instances).most_common()}')

        new_questions = []
        new_labels = []

        for instance in instances:
            new_question = {}
            new_question['id'] = instance['id']
            new_question['question'] = instance['question']['stem']
            new_question['answer_options'] = [k['text'] for k in instance['question']['choices']]

            new_questions.append(new_question)
            new_labels.append(ord(instance['answerKey'])-64)

        args.output_dir.mkdir(parents=True, exist_ok=True)
        with (args.output_dir / f'{out_split}.jsonl').open('w') as file:
            for question in new_questions:
                file.write(f"{json.dumps(question, ensure_ascii=False)}\n")
        with (args.output_dir / f'{out_split}-labels.lst').open('w') as file:
            for label in new_labels:
                file.write(f"{label}\n")


if __name__ == '__main__':
    main()
