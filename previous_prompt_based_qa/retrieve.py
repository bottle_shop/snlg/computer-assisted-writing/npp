import csv
import ast
import argparse
import json
from pathlib import Path

from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet
wordnet_lemmatizer = WordNetLemmatizer()

key_dict = {}

with open('GenericsKB-Best.tsv', encoding="utf-8") as f:
    # Skip the header
    next(f)

    read_tsv = csv.reader(f, delimiter="\t")

    for id_, row in enumerate(read_tsv):

        quantifier = row[2]
        quantifier_frequency = ""
        quantifier_number = ""

        if quantifier != "":
            quantifier = ast.literal_eval(quantifier)
            if "frequency" in quantifier.keys():
                quantifier_frequency = quantifier["frequency"]
            if "number" in quantifier.keys():
                quantifier_number = quantifier["number"]

        if row[1] not in key_dict:
            key_dict[row[1]] = []
            key_dict[row[1]].append({
                "source": row[0],
                "term": row[1],
                "quantifier_frequency": quantifier_frequency,
                "quantifier_number": quantifier_number,
                "generic_sentence": row[3],
                "score": row[4],
            })

def main() -> None:
    p = argparse.ArgumentParser(description=__doc__)
    p.add_argument('--input-dir', type=Path, default=Path('data_dir','riddle_sense'))
    p.add_argument('--output-dir', type=Path, default=Path('task_data', 'riddle_sense_context'))
    p.add_argument('--subset', type=float, default=None)
    args = p.parse_args()

    for in_split, out_split in (('train', 'train'), ('dev', 'dev'), ('test', 'test')):
        instance_path = args.input_dir / f'{in_split}.jsonl'

        with instance_path.open(encoding='utf-8-sig') as file:
            instances = [json.loads(line) for line in file]

        #print(f'{in_split}: {Counter(instance["question"] for instance in instances).most_common()}')

        new_questions = []
        new_labels = []
        total = 0
        no = 0
        for instance in instances:
            new_question = {}
            new_question['id'] = instance['id']
            new_question['question'] = instance['question']['stem']
            new_question['answer_options'] = [k['text'] for k in instance['question']['choices']]
            new_question['context'] = []
            for answer_option in new_question['answer_options']:
                total += 1
                answer_key = wordnet_lemmatizer.lemmatize(answer_option.lower())
                if answer_key in key_dict:
                    new_question['context'].append(key_dict[answer_key][0]['generic_sentence'])
                else:
                    no += 1
                    print(answer_key)
            new_questions.append(new_question)
            new_labels.append(ord(instance['answerKey'])-64)
        print(no, total)
        args.output_dir.mkdir(parents=True, exist_ok=True)
        with (args.output_dir / f'{out_split}.jsonl').open('w') as file:
            for question in new_questions:
                file.write(f"{json.dumps(question, ensure_ascii=False)}\n")
        with (args.output_dir / f'{out_split}-labels.lst').open('w') as file:
            for label in new_labels:
                file.write(f"{label}\n")

if __name__ == '__main__':
    main()

