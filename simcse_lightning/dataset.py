import os
from typing import Mapping, Sequence, Tuple, Optional
import torch
from transformers import BatchEncoding, PreTrainedTokenizer, AutoTokenizer
from datasets import load_dataset

Batch = Mapping[str, Sequence[BatchEncoding]]

class SimCSEDataset:
    def __init__(
        self,
        tokenizer: PreTrainedTokenizer,
        data_dir: str,
        data_file: str,
        max_seq_length: int = 32,
        mlm: bool = True,
        mlm_probability: float = 0.15
    ):
        self.data_dir = data_dir
        self.data_file = data_file
        self.max_seq_length = max_seq_length
        self.tokenizer = tokenizer
        self.mlm = mlm
        self.mlm_probability = mlm_probability
        self.inputs = []
        self._build()

    def __len__(self) -> int:
        return len(self.inputs)

    def __getitem__(self, index: int) -> Batch:
        input_ids = self.inputs[index]["input_ids"].squeeze()
        attention_mask = self.inputs[index]["attention_mask"].squeeze()
        token_type_ids = self.inputs[index]["token_type_ids"].squeeze()
        if self.mlm:
            mlm_input_ids = self.inputs[index]["mlm_input_ids"].squeeze()
            mlm_labels = self.inputs[index]["mlm_labels"].squeeze()
            return {
                "input_ids": input_ids,
                "attention_mask": attention_mask,
                "token_type_ids": token_type_ids,
                "mlm_input_ids": mlm_input_ids,
                "mlm_labels": mlm_labels,
            }
        else:
            return {
                "input_ids": input_ids,
                "attention_mask": attention_mask,
                "token_type_ids": token_type_ids,
            }

    def read_file(
        self,
        train_path: str
    ):
        data_file = {"train": train_path}
        extension = train_path.split(".")[-1]

        # Unsupervised
        if extension == "txt":
            extension = "text"

        # Supervised
        if extension == "csv":
            datasets = load_dataset(extension, data_files=data_file, cache_dir="./data/", delimiter=",")
        else:
            datasets = load_dataset(extension, data_files=data_file, cache_dir="./data/")

        return datasets

    def mask_tokens(
        self,
        inputs: torch.Tensor,
        special_tokens_mask:
        Optional[torch.Tensor] = None
    ) -> Tuple[torch.Tensor, torch.Tensor]:
        """
        Prepare masked tokens inputs/labels for masked language modeling: 80% MASK, 10% random, 10% original.
        """
        labels = inputs.clone()
        # We sample a few tokens in each sequence for MLM training (with probability `self.mlm_probability`)
        probability_matrix = torch.full(labels.shape, self.mlm_probability)
        if special_tokens_mask is None:
            special_tokens_mask = [
                self.tokenizer.get_special_tokens_mask(val, already_has_special_tokens=True) for val in
                labels.tolist()
            ]
            special_tokens_mask = torch.tensor(special_tokens_mask, dtype=torch.bool)
        else:
            special_tokens_mask = special_tokens_mask.bool()

        probability_matrix.masked_fill_(special_tokens_mask, value=0.0)
        masked_indices = torch.bernoulli(probability_matrix).bool()
        labels[~masked_indices] = -100  # We only compute loss on masked tokens

        # 80% of the time, we replace masked input tokens with tokenizer.mask_token ([MASK])
        indices_replaced = torch.bernoulli(torch.full(labels.shape, 0.8)).bool() & masked_indices
        inputs[indices_replaced] = self.tokenizer.convert_tokens_to_ids(self.tokenizer.mask_token)

        # 10% of the time, we replace masked input tokens with random word
        indices_random = torch.bernoulli(torch.full(labels.shape, 0.5)).bool() & masked_indices & ~indices_replaced
        random_words = torch.randint(len(self.tokenizer), labels.shape, dtype=torch.long)
        inputs[indices_random] = random_words[indices_random]

        # The rest of the time (10% of the time) we keep the masked input tokens unchanged
        return inputs, labels

    def _build(self) -> None:
        self.encode_file(
            self.tokenizer,
            os.path.join(self.data_dir, self.data_file),
            self.max_seq_length
        )

    def encode_file(
        self,
        tokenizer: PreTrainedTokenizer,
        train_data_path: str,
        max_length: int,
    ) -> Sequence[BatchEncoding]:

        datasets = self.read_file(train_data_path)
        # Set different column name for each dataset setting
        column_names = datasets["train"].column_names
        # Temporary variable for case of hard negative
        sent2_cname = None
        if len(column_names) == 2:
            # Supervised datasets - Pair datasets
            sent0_cname = column_names[0]
            sent1_cname = column_names[1]
        elif len(column_names) == 3:
            # Supervised datasets - Pair datasets with hard negatives
            sent0_cname = column_names[0]
            sent1_cname = column_names[1]
            sent2_cname = column_names[2]
        elif len(column_names) == 1:
            # Unsupervised datasets
            sent0_cname = column_names[0]
            sent1_cname = column_names[0]
        else:
            raise NotImplementedError

        def prepare_features(examples):
            total_size = len(examples[sent0_cname])

            # Filtering "None" fields
            for idx in range(total_size):
                if examples[sent0_cname][idx] is None:
                    examples[sent0_cname][idx] = " "
                if examples[sent1_cname][idx] is None:
                    examples[sent1_cname][idx] = " "
                # If hard negative exists
                if sent2_cname is not None:
                    if examples[sent2_cname][idx] is None:
                        examples[sent2_cname][idx] = " "

            sentences = examples[sent0_cname] + examples[sent1_cname]

            # If hard negative exists
            if sent2_cname is not None:
                sentences += examples[sent2_cname]

            sent_features = tokenizer(
                sentences,
                max_length=max_length,
                truncation=True,
                padding=False,
            )

            # Divide into pairs or triples.
            features = {}
            if sent2_cname is not None:
                for key in sent_features:
                    features[key] = [
                        [sent_features[key][i], sent_features[key][i + total_size], sent_features[key][i + total_size * 2]] for i
                        in range(total_size)]
            else:
                for key in sent_features:
                    features[key] = [[sent_features[key][i], sent_features[key][i + total_size]] for i in range(total_size)]

            return features

        train_dataset = datasets["train"].map(
            prepare_features,
            batched=True,
            num_proc=4, #TODO
            remove_columns=column_names,
            load_from_cache_file=False, #TODO
        )

        special_keys = ['input_ids', 'attention_mask', 'token_type_ids', 'mlm_input_ids', 'mlm_labels']
        for data in train_dataset:
            num_sent = len(data['input_ids'])
            flat_data = {k: data[k] if k in special_keys else data[k] for k in data}

            data_input = self.tokenizer.pad(
                flat_data,
                padding='max_length',
                max_length=self.max_seq_length,
                pad_to_multiple_of=None,
                return_tensors="pt",
            )

            if self.mlm:
                data_input["mlm_input_ids"], data_input["mlm_labels"] = self.mask_tokens(data_input["input_ids"])

            data_input = {k: data_input[k].view(1, num_sent, -1) if k in special_keys else data_input[k].view(1, num_sent, -1)[:, 0] for k in data_input}
            if "label" in data_input:
                data_input["labels"] = data_input["label"]
                del data_input["label"]
            if "label_ids" in data_input:
                data_input["labels"] = data_input["label_ids"]
                del data_input["label_ids"]

            self.inputs.append(data_input)

class STSBDataset:
    def __init__(
        self,
        tokenizer: PreTrainedTokenizer,
        max_seq_length: int = 32,
    ):
        self.max_seq_length = max_seq_length
        self.tokenizer = tokenizer
        self.inputs = []
        self._build()

    def __len__(self) -> int:
        return len(self.inputs)

    def __getitem__(self, index: int) -> Batch:
        input_ids = self.inputs[index]["input_ids"].squeeze()
        attention_mask = self.inputs[index]["attention_mask"].squeeze()
        token_type_ids = self.inputs[index]["token_type_ids"].squeeze()
        labels = self.inputs[index]["labels"].squeeze()
        return {
            "input_ids": input_ids,
            "attention_mask": attention_mask,
            "token_type_ids": token_type_ids,
            "labels": labels,
        }

    def read_file(
        self
    ):
        datasets = load_dataset('glue','stsb')
        return datasets

    def _build(self) -> None:
        self.encode_file(
            self.tokenizer,
            self.max_seq_length
        )

    def encode_file(
        self,
        tokenizer: PreTrainedTokenizer,
        max_length: int,
    ) -> Sequence[BatchEncoding]:
        datasets = self.read_file()['validation']
        column_names = datasets.column_names
        sent0_cname = column_names[2]
        sent1_cname = column_names[3]
        label_cname = column_names[1]

        def prepare_features(examples):
            total_size = len(examples[sent0_cname])
            sentences = examples[sent0_cname] + examples[sent1_cname]
            sent_features = tokenizer(
                sentences,
                max_length=max_length,
                truncation=True,
                padding=False,
            )

            # Divide into pairs or triples.
            features = {}
            for key in sent_features:
                features[key] = [[sent_features[key][i], sent_features[key][i + total_size]] for i in range(total_size)]
            features['labels'] = examples[label_cname]
            return features

        val_dataset = datasets.map(
            prepare_features,
            batched=True,
            num_proc=4, #TODO
            remove_columns=column_names,
            load_from_cache_file=False, #TODO
        )

        special_keys = ['input_ids', 'attention_mask', 'token_type_ids']
        for data in val_dataset:
            flat_data = {k: data[k] for k in special_keys}

            data_input = self.tokenizer.pad(
                flat_data,
                padding='max_length',
                max_length=self.max_seq_length,
                pad_to_multiple_of=None,
                return_tensors="pt",
            )

            data_input["labels"] = torch.Tensor([data["labels"]])
            self.inputs.append(data_input)
