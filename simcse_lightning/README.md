# nlp-simcse
SimCSE pytorch lightning

#TODO: Commands

````
python train.py \
--output_dir checkpoints/simcse-bert-base-32-128 \
--train_batch_size 32  \
--max_seq_length 128 \
--gradient_accumulation_steps 1
````
````
python train.py \
--output_dir checkpoints/simcse-bert-base-64-32 \
--train_batch_size 64  \
--max_seq_length 32 \
--gradient_accumulation_steps 1
````
````
python train.py \
--output_dir checkpoints/simcse-bert-base-32-32 \
--train_batch_size 32  \
--max_seq_length 32 \
--gradient_accumulation_steps 1
````
````
python train.py \
--output_dir checkpoints/simcse-bert-base-128-32 \
--train_batch_size 128  \
--max_seq_length 32 \
--gradient_accumulation_steps 1
````
````
python train.py \
--output_dir checkpoints/simcse-bert-base-256-32 \
--train_batch_size 256  \
--max_seq_length 32 \
--gradient_accumulation_steps 1
````
````
python train.py \
--output_dir checkpoints/simcse-bert-base-512-32 \
--train_batch_size 512  \
--max_seq_length 32 \
--gradient_accumulation_steps 1
````
````
python train.py \
--output_dir checkpoints/simcse-bert-base-mlm \
--train_batch_size 8 \
--gradient_accumulation_steps 1 \
--mlm
````
