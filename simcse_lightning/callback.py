import os
import pytorch_lightning as pl

class StepCheckpointCallback(pl.Callback):
    """
        Saves the model checkpoints once every n training steps.
    """
    def __init__(self, dirpath, save_every_n_steps):
        self.dirpath = dirpath
        self.save_every_n_steps = save_every_n_steps
        self.ckpt_hash_paths = dict()

    def on_batch_end(self, trainer, _):
        cur_epoch = trainer.current_epoch
        cur_global_step = trainer.global_step
        if cur_global_step % self.save_every_n_steps == 0:
            file_name = "checkpoint_" +\
                        "epoch=" + str(cur_epoch) + \
                        "-step=" + str(cur_global_step) + \
                        "-spearman=" + str(trainer.logger_connector.callback_metrics['eval_stsb_spearman'].item()) + ".ckpt"
            ckpt_path = os.path.join(self.dirpath, file_name)
            trainer.save_checkpoint(ckpt_path)

