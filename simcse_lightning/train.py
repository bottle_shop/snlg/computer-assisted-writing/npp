"""Fine-tune T5-based model."""

import argparse
import os

import pytorch_lightning as pl
import torch
from utils import set_seed, get_best_checkpoint
from simcse import Classifier
from callback import StepCheckpointCallback

def main() -> None:
    """Fine-tune SimCSE model."""
    parser = argparse.ArgumentParser()

    # Data arguments
    parser.add_argument(
        "--data_dir",
        type=str,
        default="data",
        help="Path for data files."
    )

    parser.add_argument(
        "--data_file",
        type=str,
        default="nli_for_simcse.csv",
        help="Path for data files."
    )

    parser.add_argument(
        "--max_seq_length",
        type=int,
        default=128,
        help="Maximum sequence length."
    )

    # Model arguments
    parser.add_argument(
        "--model",
        type=str,
        default="bert-base-uncased",
        help="Model name or path.",
    )

    parser.add_argument(
        "--pooler_type",
        default="cls",
        const="cls",
        nargs="?",
        choices=['cls', 'avg', 'max', 'avg_first_last'],
        help="What kind of pooler to use.",
    )

    parser.add_argument(
        "--temp",
        type=float,
        default=0.05,
        help="Temperature for softmax."
    )

    parser.add_argument(
        "--hard_negative_weight",
        type=float,
        default=0,
        help="The **logit** of weight for hard negatives (only effective if hard negatives are used)."
    )

    parser.add_argument(
        "--mlm",
        action="store_true",
        help="Whether to use MLM auxiliary objective.")

    parser.add_argument(
        "--mlm_weight",
        type=float,
        default=0.1,
        help="Weight for MLM auxiliary objective (only effective if --mlm)."
    )

    # Train Arguments
    parser.add_argument(
        "--output_dir",
        type=str,
        default="checkpoints/simcse-bert-base",
        help="Path to save the checkpoints.",
    )

    parser.add_argument(
        "--checkpoint_dir",
        type=str,
        default="",
        help="Checkpoint directory."
    )


    # Optimizer Arguments
    # You can find out more on optimization levels at
    # https://nvidia.github.io/apex/amp.html#opt-levels-and-properties
    parser.add_argument(
        "--opt_level",
        type=str,
        default="O1",
        help="Optimization level.")

    parser.add_argument(
        "--fp_16",
        action="store_true",
        help="Whether to use 16-bit precision floating point operations.",
    )

    parser.add_argument(
        "--learning_rate",
        type=float,
        default=5e-5, # Unsupervised: 3e-5
        help="Learning rate.")

    parser.add_argument(
        "--weight_decay",
        type=float,
        default=0.0,
        help="Weight decay.")

    parser.add_argument(
        "--adam_epsilon",
        type=float,
        default=1e-8,
        help="Epsilon value for Adam optimizer."
    )

    # If you enable 16-bit training then set this to a sensible value, 0.5 is a good default
    parser.add_argument(
        "--max_grad_norm",
        type=float,
        default=1.0,
        help="Maximum gradient norm value for clipping."
    )

    parser.add_argument(
        "--warmup_steps",
        type=int,
        default=0,
        help="Number of warmup steps.")

    parser.add_argument(
        "--train_batch_size",
        type=int,
        default=32,
        help="Batch size for training.")

    parser.add_argument(
        "--eval_batch_size",
        type=int,
        default=32,
        help="Batch size for evaluation."
    )

    parser.add_argument(
        "--num_train_epochs",
        type=int,
        default=3,
        help="Number of training epochs."
    )

    parser.add_argument(
        "--validate_steps",
        type=int,
        default=250,
        help="Validate Steps."
    )

    parser.add_argument(
        "--gradient_accumulation_steps",
        type=int,
        default=1,
        help="Gradient accumulation steps."
    )

    parser.add_argument(
        "--n_gpu",
        type=int,
        default=1,
        help="Number of GPUs to use for computation."
    )
    parser.add_argument(
        "--gpu_nums",
        type=str,
        default="-1",
        help='GPU IDs separated by "," to use for computation.',
    )

    parser.add_argument(
        "--seed",
        type=int,
        default=42,
        help="Manual seed value."
    )

    # TODO: Need for RoBERTa Training.
    parser.add_argument(
        "--model_parallel",
        action="store_true",
        help="Enable model parallelism."
    )

    args = parser.parse_known_args()[0]
    print(args)

    set_seed(args.seed)

    # Create a folder if output_dir doesn't exist
    if not os.path.exists(args.output_dir):
        print("Creating output directory")
        os.makedirs(args.output_dir)

    custom_checkpoint_callback = StepCheckpointCallback(
        dirpath=args.output_dir,
        save_every_n_steps=args.validate_steps
    )
    trainer_custom_callbacks = [custom_checkpoint_callback]

    if args.checkpoint_dir:
        best_checkpoint_path = get_best_checkpoint(args.checkpoint_dir)
        print(f"Using checkpoint = {best_checkpoint_path}")
        checkpoint_state = torch.load(best_checkpoint_path, map_location="cpu")
        model = Classifier(args)
        model.load_state_dict(checkpoint_state["state_dict"])
    else:
        model = Classifier(args)

    trainer = pl.Trainer(
        accumulate_grad_batches=args.gradient_accumulation_steps,
        max_epochs=args.num_train_epochs,
        precision=16 if args.fp_16 else 32,
        amp_level=args.opt_level,
        gradient_clip_val=args.max_grad_norm,
        callbacks=trainer_custom_callbacks,
        val_check_interval = args.validate_steps,
        distributed_backend="ddp" if not args.model_parallel else None,
        gpus=args.gpu_nums if not args.model_parallel else None,
    )
    trainer.fit(model)

if __name__ == "__main__":
    main()


