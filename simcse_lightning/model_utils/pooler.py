import torch.nn as nn

class Pooler(nn.Module):
    """Performs pooling on the token embeddings

    "cls"
    "avg"
    "max"
    "avg_first_last"

    """
    def __init__(self,
                 pooling_mode: str = None,
                 ):
        super().__init__()

        pooling_mode = pooling_mode.lower()
        assert pooling_mode in ['cls', 'avg', 'max', 'avg_first_last']
        self.pooling_mode_cls_token = (pooling_mode == 'cls')
        self.pooling_mode_avg_tokens = (pooling_mode == 'avg')
        self.pooling_mode_max_tokens = (pooling_mode == 'max')
        self.pooling_mode_avg_first_last_tokens = (pooling_mode == 'avg_first_last')

    def forward(self,
                attention_mask,
                outputs):

        last_hidden = outputs.last_hidden_state
        hidden_states = outputs.hidden_states

        if self.pooling_mode_cls_token:
            return last_hidden[:, 0]

        if self.pooling_mode_avg_tokens:
            return ((last_hidden * attention_mask.unsqueeze(-1)).sum(1) / attention_mask.sum(-1).unsqueeze(-1))

        if self.pooling_mode_max_tokens:
            NotImplemented
            # TODO: Need Implementation.

        if self.pooling_mode_avg_first_last_tokens:
            first_hidden = hidden_states[0]
            last_hidden = hidden_states[-1]
            pooled_result = ((first_hidden + last_hidden) / 2.0 * attention_mask.unsqueeze(-1)).sum(1) / attention_mask.sum(-1).unsqueeze(-1)
            return pooled_result

