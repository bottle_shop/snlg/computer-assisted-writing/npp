import argparse
from dataset import SimCSEDataset, STSBDataset
from pathlib import Path
import pytorch_lightning as pl
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from transformers import AdamW, AutoModel, AutoTokenizer, AutoConfig, PreTrainedTokenizer
from transformers.models.roberta.modeling_roberta import RobertaLMHead
from transformers.models.bert.modeling_bert import BertLMPredictionHead
from transformers.modeling_outputs import SequenceClassifierOutput, BaseModelOutputWithPoolingAndCrossAttentions
from transformers.modeling_utils import Conv1D

from model_utils.pooler import Pooler
from model_utils.utils import MLPLayer, Similarity

from scipy.stats import spearmanr, pearsonr
import numpy as np

def get_dataset(
    tokenizer: PreTrainedTokenizer,
    args: argparse.Namespace,
    train: bool = False,
    dev: bool = False,
):
    if train:
        return SimCSEDataset(
            tokenizer=tokenizer,
            data_dir=args.data_dir,
            data_file=args.data_file,
            max_seq_length=args.max_seq_length,
            mlm=args.mlm,
            mlm_probability=0.15
        )
    if dev:
        return STSBDataset(
            tokenizer=tokenizer,
            max_seq_length=args.max_seq_length
        )

class Classifier(pl.LightningModule):  # type: ignore[misc]

    def __init__(self, hparams):
        super(Classifier, self).__init__()
        if isinstance(hparams, dict):
            hparams = argparse.Namespace(**hparams)
        self.save_hyperparameters(hparams)
        print("Model params: ", self.hparams)
        self.root_path = Path(__file__).parent.absolute()

        # Load Transformer model_utils from cache files (encoder and tokenizer)
        self.model = AutoModel.from_pretrained(self.hparams.model, cache_dir=self.root_path / "model_cache")
        self.tokenizer = AutoTokenizer.from_pretrained(self.hparams.model, cache_dir=self.root_path / "model_cache", use_fast=False)
        self.config = AutoConfig.from_pretrained(self.hparams.model, cache_dir=self.root_path / "model_cache")
        self.model.train()

        if self.hparams["mlm"]:
            if 'roberta' in self.hparams.model:
                self.lm_head = RobertaLMHead(self.config)
            elif 'bert' in self.hparams.model:
                self.lm_head = BertLMPredictionHead(self.config)
            # self.lm_head.load_state_dict(self.model.cls.predictions.state_dict())
            self.lm_head.train()

        # Layer settings
        self.pooler_type = self.hparams.pooler_type # 'cls', 'avg', 'max', 'avg_first_last'
        self.pooler = Pooler(self.hparams.pooler_type)
        self.mlp = MLPLayer(self.config.hidden_size)
        self.sim = Similarity(temp = self.hparams.temp)

        ## Init weights
        self.init_weights(self.pooler)
        self.init_weights(self.mlp)
        self.init_weights(self.sim)

        # Create the one layer feed forward neural net for classification purpose after the encoder
        self.loss = nn.CrossEntropyLoss()

    # Initialize the weight
    def init_weights(self, module):
        if isinstance(module, (nn.Linear, nn.Embedding, Conv1D)):
            # Slightly different from the TF version which uses truncated_normal for initialization
            # cf https://github.com/pytorch/pytorch/pull/5617
            print(self.config.initializer_range)
            module.weight.data.normal_(mean=0.0, std=self.config.initializer_range)
            if isinstance(module, (nn.Linear, Conv1D)) and module.bias is not None:
                module.bias.data.zero_()

    def contrastive_learning(self, batch):
        batch["token_type_ids"] = None if "roberta" in self.hparams.model else batch["token_type_ids"]
        batch_size = batch["input_ids"].size(0)

        # Number of sentences in one instance
        # 2: pair instance; 3: pair instance with a hard negative
        num_sent = batch["input_ids"].size(1)

        # Flatten input for encoding
        input_ids = batch["input_ids"].view((-1, batch["input_ids"].size(-1)))  # (bs * num_sent, len)
        attention_mask = batch["attention_mask"].view((-1, batch["attention_mask"].size(-1)))  # (bs * num_sent, len)
        token_type_ids = batch["token_type_ids"].view((-1, batch["token_type_ids"].size(-1)))  # (bs * num_sent, len)

        # Get raw embeddings
        outputs = self.model(
            input_ids,
            attention_mask=attention_mask,
            token_type_ids=token_type_ids,
            output_hidden_states=True if self.pooler_type == 'avg_first_last' else False,
            return_dict=True,
        )

        # MLM auxiliary objective
        if self.hparams["mlm"]:
            mlm_input_ids = batch["mlm_input_ids"].view((-1, batch["mlm_input_ids"].size(-1)))
            mlm_outputs = self.model(
                mlm_input_ids,
                attention_mask=attention_mask,
                token_type_ids=token_type_ids,
                output_hidden_states=True if self.pooler_type == 'avg_first_last' else False,
                return_dict=True,
            )

        # Pooling
        pooler_output = self.pooler(attention_mask, outputs)
        pooler_output = pooler_output.view((batch_size, num_sent, pooler_output.size(-1)))  # (bs, num_sent, hidden)

        if self.pooler_type == "cls":
            pooler_output = self.mlp(pooler_output)

        # Separate representation
        z1, z2 = pooler_output[:, 0], pooler_output[:, 1]
        cos_sim = self.sim(z1.unsqueeze(1), z2.unsqueeze(0))
        # Hard negative
        if num_sent >= 3:
            z3 = pooler_output[:, 2]
            z1_z3_cos = self.sim(z1.unsqueeze(1), z3.unsqueeze(0))
            #cos_sim = torch.cat([cos_sim, z1_z3_cos], 1)
            cos_sim = z1_z3_cos

        labels = torch.arange(cos_sim.size(0)).long().to(self.device)

        # Calculate loss with hard negatives
        if num_sent == 3:
            # Note that weights are actually logits of weights
            z3_weight = self.hparams.hard_negative_weight
            weights = torch.tensor(
                [[0.0] * (cos_sim.size(-1) - z1_z3_cos.size(-1)) + [0.0] * i + [z3_weight] + [0.0] * (
                        z1_z3_cos.size(-1) - i - 1) for i in range(z1_z3_cos.size(-1))]
            ).to(self.device)
            cos_sim = cos_sim + weights

        loss = self.loss(cos_sim, labels)

        # Calculate loss for MLM
        if self.hparams["mlm"]:
            mlm_labels = batch["mlm_labels"].view(-1, batch["mlm_labels"].size(-1))
            prediction_scores = self.lm_head(mlm_outputs.last_hidden_state)
            masked_lm_loss = self.loss(prediction_scores.view(-1, self.config.vocab_size), mlm_labels.view(-1))
            loss = loss + self.hparams.mlm_weight * masked_lm_loss

        return SequenceClassifierOutput(
            loss=loss,
            logits=cos_sim,
            hidden_states=outputs.hidden_states,
            attentions=outputs.attentions
        )

    def inference(self, batch):
        outputs = self.model(
            batch["input_ids"],
            attention_mask=batch["attention_mask"],
            token_type_ids=batch["token_type_ids"],
            output_hidden_states=True if self.pooler_type == 'avg_first_last' else False
        )

        pooler_output = self.pooler(batch["attention_mask"], outputs)

        if self.pooler_type == "cls":
            pooler_output = self.mlp(pooler_output)

        return BaseModelOutputWithPoolingAndCrossAttentions(
            pooler_output=pooler_output,
            last_hidden_state=outputs.last_hidden_state,
            hidden_states=outputs.hidden_states,
        )

    def validation(self, batch):
        batch["token_type_ids"] = None if "roberta" in self.hparams.model else batch["token_type_ids"]

        batch1 = {}
        batch2 = {}

        batch1["input_ids"], batch2["input_ids"] = batch["input_ids"][:, 0], batch["input_ids"][:, 1]
        batch1["attention_mask"], batch2["attention_mask"] = batch["attention_mask"][:, 0], batch["attention_mask"][:, 1]
        batch1["token_type_ids"], batch2["token_type_ids"] = batch["token_type_ids"][:, 0], batch["token_type_ids"][:, 1]

        batch1_output = self.inference(batch1)
        batch2_output = self.inference(batch2)

        return BaseModelOutputWithPoolingAndCrossAttentions(
            pooler_output=batch1_output.pooler_output
        ), BaseModelOutputWithPoolingAndCrossAttentions(
            pooler_output=batch2_output.pooler_output
        )

    def forward(self, batch, train:bool=False, dev:bool=False, test:bool=False):
        if train:
            return self.contrastive_learning(batch)
        if dev:
            return self.validation(batch)
        if test:
            return self.inference(batch)

    # Data loader methods to return train and validation data sets
    def train_dataloader(self):
        train_dataset = get_dataset(tokenizer=self.tokenizer, args=self.hparams, train=True)
        dataloader = DataLoader(
            train_dataset,
            batch_size=self.hparams.train_batch_size,
            drop_last=True,
            shuffle=True,
            num_workers=4, #TODO
        )
        return dataloader

    def val_dataloader(self):
        val_dataset = get_dataset(tokenizer=self.tokenizer, args=self.hparams, dev=True)
        dataloader = DataLoader(
            val_dataset,
            batch_size=self.hparams.eval_batch_size,
            drop_last=True,
            shuffle=True,
            num_workers=4, #TODO
        )
        return dataloader

    # Extend PyTorch Lightning methods
    def training_step(self, batch, batch_idx):
        output = self.forward(batch, train=True)
        loss = output.loss
        return {"loss": loss}

    def training_step_end(self, training_step_outputs):
        return training_step_outputs

    def validation_step(self, batch, batch_idx: int):
        output1, output2 = self.forward(batch, dev=True)
        z1, z2 = output1.pooler_output, output2.pooler_output
        sys_scores = []

        def cosine(u, v):
            return np.dot(u, v) / (np.linalg.norm(u) * np.linalg.norm(v))

        for kk in range(z2.shape[0]):
            sys_score = np.nan_to_num(cosine(np.nan_to_num(z1[kk].cpu()), np.nan_to_num(z2[kk].cpu())))
            sys_scores.append(sys_score)

        return {"sys_score": sys_scores, "labels": batch["labels"].cpu()}

    def validation_epoch_end(self, outputs):
        sys_scores = []
        labels = []
        for out in outputs:
            sys_scores.extend(out["sys_score"])
            labels.extend(out["labels"])

        stsb_spearman = spearmanr(sys_scores, labels)
        stsb_spearman = torch.tensor([stsb_spearman[0]])
        self.log("eval_stsb_spearman", stsb_spearman, prog_bar=True, on_epoch=True)

        return {"eval_stsb_spearman": stsb_spearman}

    def configure_optimizers(self):
        optimizer = AdamW(self.parameters(),
                          lr=float(self.hparams.learning_rate),
                          eps=float(self.hparams.adam_epsilon))
        return optimizer
